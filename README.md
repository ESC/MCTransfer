MCTransfer
==========

Simple protobuf based example.

Quick Start
-----------

Out of source cmake build.

```
git clone https://gitlab.com/ESC/MCTransfer.git
mkdir mctransfer_build &&  cd mctransfer_build
cmake ../MCTransfer -DCMAKE_INSTALL_PREFIX=../mctransfer_install #or wherever
make 
make install
```

To add your own proto files to the library, just put them in the `protofiles` 
directory.

Note only python and C++ libraries are generated. 

Features
--------

### C++ Library

There is a cmake configured  header in `MCTransferConfig.h` that can be 
configured to store useful information.

For example, later on if we want to develop a mechanism to automatically 
extract foreign proto files stored at the start of the file, it can be checked 
against the currenlty configured. That is in `MCTransferConfig.h` you will find

```
#define MCTRANSFER_PROTOFILENAMES   "ProEIC;ProMC;ProMCDescription;ProMCHeader;ProMCStat"
```



TODO
----

0. Use protobuf version 3 (fix version 2 warnings)
1. Add examples
2. Add protobuf to gzipped file 
3. ...
4. profit


Installed Files
---------------

```
.
├── include
│   └── MCTransfer
│       ├── MCTransferConfig.h
│       ├── ProEIC.pb.h
│       ├── ProMCDescription.pb.h
│       ├── ProMCHeader.pb.h
│       ├── ProMC_NLO.pb.h
│       ├── ProMC.pb.h
│       └── ProMCStat.pb.h
├── install_tree.txt
├── lib
│   ├── libMCTransfer.so -> libMCTransfer.so.1
│   ├── libMCTransfer.so.1 -> libMCTransfer.so.1.6.0
│   ├── libMCTransfer.so.1.6.0
│   └── python2.7
│       └── site-packages
│           ├── MCTransfer
│           │   ├── ProEIC_pb2.py
│           │   ├── ProEIC_pb2.pyc
│           │   ├── ProMCDescription_pb2.py
│           │   ├── ProMCDescription_pb2.pyc
│           │   ├── ProMCHeader_pb2.py
│           │   ├── ProMCHeader_pb2.pyc
│           │   ├── ProMC_NLO_pb2.py
│           │   ├── ProMC_NLO_pb2.pyc
│           │   ├── ProMC_pb2.py
│           │   ├── ProMC_pb2.pyc
│           │   ├── ProMCStat_pb2.py
│           │   └── ProMCStat_pb2.pyc
│           └── MCTransfer-0.0.0-py2.7.egg-info
└── share
    └── MCTRANSFER
        └── protofiles
            ├── ProEIC.proto
            ├── ProMCDescription.proto
            ├── ProMCHeader.proto
            ├── ProMC_NLO.proto
            ├── ProMC.proto
            └── ProMCStat.proto
```

